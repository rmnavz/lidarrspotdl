import os
import requests
import argparse
import subprocess
import json
import shutil
import spotipy
import re
import logging
from spotipy.oauth2 import SpotifyClientCredentials

# Set up logging to a file
log_file_path = "/logs/run_spotdl.log"
logging.basicConfig(filename=log_file_path, level=logging.INFO)

def sanitize_directory_name(name):
    # Replace spaces and special characters with underscores
    sanitized_name = re.sub(r'[^a-zA-Z0-9_\-]', '_', name)
    return sanitized_name

def extract_spotify_urls_and_download(download_dir):
    lidarr_url = os.getenv("LIDARR_URL", "http://your-lidarr-ip:port")
    api_key = os.getenv("API_KEY", "your-lidarr-api-key")
    endpoint = f"{lidarr_url}/api/v1/wanted/missing?apiKey={api_key}"

    response = requests.get(endpoint)
    data = response.json()

    downloaded_directories = []

    for record in data.get("records", []):
        artist_links = record.get("artist", {}).get("links", [])

        for link in artist_links:
            if link.get("name") == "spotify":
                spotify_artist_url = link.get("url")
                print(f"Spotify URL for artist {record['artist']['artistName']}: {spotify_artist_url}")

                # Pick the monitored release
                monitored_release = None
                for release in record.get("releases", []):
                    if release.get("monitored", False):
                        monitored_release = release
                        break
                
                if monitored_release:
                    # Extract album name from the monitored release
                    album_name = monitored_release.get("title", "Unknown Album")

                    # Sanitize the directory name
                    sanitized_artist_name = sanitize_directory_name(record['artist']['artistName'])

                    if album_name == "Unknown Album":
                        # Run search_spotify to get the track URL
                        track_url = search_spotify(record['artist']['artistName'], record['title'])  # Assuming the first track in the list
                    else:
                        # Run search_spotify to get the track URL
                        track_url = search_spotify(record['artist']['artistName'], record['title'], album_name)  # Assuming the first track in the list
                    
                    if track_url:
                        sanitized_artist_name = sanitize_directory_name(record['artist']['artistName'])
                        # Download track using SpotDL
                        download_command = f"spotdl {track_url} --output {download_dir}/{sanitized_artist_name}_spotdl"

                        try:
                            # Check available disk space before downloading
                            if enough_free_space(download_dir):
                                subprocess.run(download_command, shell=True, check=True)
                                downloaded_directory = f"{download_dir}/{record['artist']['artistName']}"
                                downloaded_directories.append(downloaded_directory)
                                print(f"Downloaded track for artist {record['artist']['artistName']}")
                            else:
                                print(f"Not enough free space to download track for artist {record['artist']['artistName']}")
                        except subprocess.CalledProcessError as e:
                            print(f"Error downloading track for artist {record['artist']['artistName']}: {e}")            

    # Print the downloaded directories as JSON to stdout
    logging.info(f"Downloaded directories: {downloaded_directories}")
    print(json.dumps(downloaded_directories))

def search_spotify(artist_url, song_name, album_name=None):
    # Set your Spotify API credentials
    client_id = os.getenv("spotify_client_id", "your-client-id")
    client_secret = os.getenv("spotify_client_secret", "your-client-secret")

    # Authenticate with Spotify API
    client_credentials_manager = SpotifyClientCredentials(client_id=client_id, client_secret=client_secret)
    sp = spotipy.Spotify(client_credentials_manager=client_credentials_manager)

    # Extract artist ID from the Spotify URL
    artist_id = artist_url.split(':')[-1]

    # Construct the basic query
    basic_query = f'artist:{artist_id}'

    # Include album information if available
    if album_name:
        basic_query += f' album:{album_name}'
    else:
        basic_query += f' track:{song_name}'

    # Search for the song
    print(f"Spotipy Query: {basic_query}")
    

    # Print the first result
    if album_name:
        results = sp.search(q=basic_query, type='album', limit=1)
        if results['albums']['items']:
            album_url = results['albums']['items'][0]['external_urls']['spotify']
            print(f"Found album '{album_name}' by {artist_id} on Spotify.")
            print(f"Spotify Album URL: {album_url}")
            return album_url
        else:
            print(f"No results found for '{song_name}' by {artist_id} on Spotify.")
            return None
    else:
        results = sp.search(q=basic_query, type='track', limit=1)
        if results['tracks']['items']:
            track_url = results['tracks']['items'][0]['external_urls']['spotify']
            print(f"Found '{song_name}' by {artist_id} on Spotify.")
            print(f"Spotify URL: {track_url}")
            return track_url
        else:
            print(f"No direct track match for '{song_name}' by {artist_id} on Spotify.")
            return None

def enough_free_space(directory, required_space=500):  # Specify required space in megabytes
    total, used, free = shutil.disk_usage(directory)
    return free >= required_space * 1024 * 1024  # Convert megabytes to bytes

# Parse command line arguments
parser = argparse.ArgumentParser(description='Extract Spotify URLs and download with SpotDL')
parser.add_argument('--download-dir', type=str, default='/downloads/music', help='Download directory')
args = parser.parse_args()

# Run extraction and download
extract_spotify_urls_and_download(args.download_dir)
