#!/bin/sh

LOCKFILE="/var/run/run_spotdl.lock"
LOGFILE="/logs/run_spotdl.log"

# Check if the lock file exists
if [ -e "$LOCKFILE" ]; then
    echo "$(date): Script is already running. Exiting." >> "$LOGFILE"
    exit 1
fi

# Redirect stdout to the log file
exec >> "$LOGFILE" 2>&1

# Create the lock file
touch "$LOCKFILE"

# Run SpotDL with Lidarr API and capture the output
DOWNLOAD_DIR="${DOWNLOAD_DIR:-/downloads/music}"  # Set your desired download directory

# Check if Python scripts exist
if [ ! -e "/app/extract_spotify_urls.py" ] || [ ! -e "/app/import_lidarr.py" ]; then
    echo "$(date): Required Python scripts not found. Exiting." >> "$LOGFILE"
    rm "$LOCKFILE"
    exit 1
fi

downloaded_directories=$(python /app/extract_spotify_urls.py --download-dir $DOWNLOAD_DIR)

# Check the return code of the Python script
if [ $? -ne 0 ]; then
    echo "$(date): Error running extract_spotify_urls.py. Exiting." >> "$LOGFILE"
    rm "$LOCKFILE"
    exit 1
fi

# Process the downloaded directories as needed
echo "$(date): Downloaded directories: $downloaded_directories" >> "$LOGFILE"

# Run the second Python script
python /app/import_lidarr.py "$downloaded_directories" 2>> "$LOGFILE"

# Check the return code of the Python script
if [ $? -ne 0 ]; then
    echo "$(date): Error running import_lidarr.py. Exiting." >> "$LOGFILE"
    rm "$LOCKFILE"
    exit 1
fi

# Remove the lock file when the script is done
rm "$LOCKFILE"
echo "$(date): Script completed successfully." >> "$LOGFILE"