# Lidarr-SpotDL Integration

<!-- ![Lidarr-SpotDL Logo](link-to-your-logo.png) -->

## Overview

This tool facilitates the integration of SpotDL with Lidarr, providing an automated solution for downloading music tracks from Spotify and importing them into Lidarr for organization. The integration is encapsulated in a Docker container for easy deployment.

## Features

- **SpotDL Integration**: Automatically fetches Spotify URLs from Lidarr's wanted missing list and downloads tracks using SpotDL.
- **Lidarr Import**: Imports the downloaded tracks back into Lidarr for seamless organization.
- **Customizable Configuration**: Easily configure the Lidarr server URL, API key, and cron schedule through environment variables.
- **Dockerized Solution**: Packaged as a Docker container for simplified deployment and dependency management.

## Usage

### Using `docker run`:

1. **Pull the Docker image from the GitHub Container Registry.**

    ```bash
    docker pull ghcr.io/your-username/lidarr-spotdl-integration:latest
    ```

2. **Run the Docker container with environment variables.**

    ```bash
    docker run -d \
      -e LIDARR_URL=http://your-lidarr-ip:port \
      -e API_KEY=your-lidarr-api-key \
      -e CRON_SCHEDULE="0 0 * * *" \
      registry.gitlab.com/rmnavz/lidarrspotdl:latest
    ```

### Using `docker-compose`:

1. **Create a `docker-compose.yml` file.**

    ```yaml
    version: '3'
    services:
      lidarr-spotdl-integration:
        image: registry.gitlab.com/rmnavz/lidarrspotdl:latest
        environment:
          - LIDARR_URL=http://your-lidarr-ip:port
          - API_KEY=your-lidarr-api-key
          - CRON_SCHEDULE="0 0 * * *"
    ```

2. **Run the Docker Compose configuration.**

    ```bash
    docker-compose up -d
    ```

## Environment Variables

- **LIDARR_URL**: The URL of your Lidarr server (default: http://your-lidarr-ip:port).
- **API_KEY**: Your Lidarr API key.
- **CRON_SCHEDULE**: Cron schedule for SpotDL execution (default: "0 0 * * *" - daily at midnight).

## Notes

- Ensure your Lidarr server is accessible from the Docker container.
- Customize the setup as needed for additional configurations or specific requirements.

## Contribution

Contributions are welcome! Feel free to open issues, submit pull requests, or provide feedback.

<!-- ## License

This project is licensed under the [MIT License](LICENSE). -->
