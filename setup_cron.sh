#!/bin/sh

# Set the default cron schedule (every 12 hours)
CRON_SCHEDULE="${CRON_SCHEDULE:-30 * * * *} "

# Set up a cron job to run the run_spotdl.sh script at a specified interval
echo "${CRON_SCHEDULE}/bin/sh /app/run_spotdl.sh" > /etc/crontab
crontab /etc/crontab

# Start cron in the foreground
cron -f
