## [1.4.1](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.4.0...1.4.1) (2023-12-04)


### Bug Fixes

* minor adjustment to scripts ([6a9b5e7](https://gitlab.com/rmnavz/lidarrspotdl/commit/6a9b5e757240be10d1956077626980f787dbbf9e))

## [1.4.0](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.3.1...1.4.0) (2023-12-02)


### Features

* sanitize directory ([ba39ba2](https://gitlab.com/rmnavz/lidarrspotdl/commit/ba39ba26382792c297d55b4b0055fb841cbaced2))

## [1.3.1](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.3.0...1.3.1) (2023-12-01)


### Bug Fixes

* remove default cron on dockerfile ([dee5391](https://gitlab.com/rmnavz/lidarrspotdl/commit/dee5391a6aef290e3f90636f1f558e04bae7696a))

## [1.3.0](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.2.0...1.3.0) (2023-12-01)


### Features

* create lockfile ([47ab32a](https://gitlab.com/rmnavz/lidarrspotdl/commit/47ab32a2a6dc61de441dca8f159bc6488b5c5110))
* install run-one on docker image ([1f2ae99](https://gitlab.com/rmnavz/lidarrspotdl/commit/1f2ae99936b45cc78c1fb4209d21db1d4a8d3709))

## [1.2.0](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.1.1...1.2.0) (2023-12-01)


### Features

* add space free check ([9e588b2](https://gitlab.com/rmnavz/lidarrspotdl/commit/9e588b2c5330ce05960e2160e9fc789787045722))

## [1.1.1](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.1.0...1.1.1) (2023-12-01)


### Bug Fixes

* removed rm since manual import not working. ([e2fdea8](https://gitlab.com/rmnavz/lidarrspotdl/commit/e2fdea8e45b96ba3676a4d61fe6ecd7b698fe484))

## [1.1.0](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.5...1.1.0) (2023-12-01)


### Features

* delete files after import ([f9db6bd](https://gitlab.com/rmnavz/lidarrspotdl/commit/f9db6bde6a3f46c4ae280fe9e803d64019773f4e))

## [1.0.5](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.4...1.0.5) (2023-12-01)


### Bug Fixes

* remove demo and fix import script ([469854f](https://gitlab.com/rmnavz/lidarrspotdl/commit/469854fe620c9d3c22667682713c65ff3158bdd0))

## [1.0.4](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.3...1.0.4) (2023-12-01)


### Bug Fixes

* spotipy search query ([7e44939](https://gitlab.com/rmnavz/lidarrspotdl/commit/7e449390e41207a2725fcf5fe20eee8642868b04))

## [1.0.3](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.2...1.0.3) (2023-12-01)


### Bug Fixes

* download track url ([9adaf46](https://gitlab.com/rmnavz/lidarrspotdl/commit/9adaf467893415c5ddedab275b0caba6cefe244a))

## [1.0.2](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.1...1.0.2) (2023-12-01)


### Bug Fixes

* environment variable setup ([3d6496c](https://gitlab.com/rmnavz/lidarrspotdl/commit/3d6496ccdf969dd42fb6d472ddaf669baf083a87))

## [1.0.1](https://gitlab.com/rmnavz/lidarrspotdl/compare/1.0.0...1.0.1) (2023-12-01)


### Bug Fixes

* install ffmpeg ([a4efbc0](https://gitlab.com/rmnavz/lidarrspotdl/commit/a4efbc0b7917883c49406ea0d5a9aab9fb505a21))

## [1.0.0](https://gitlab.com/rmnavz/lidarrspotdl/compare/...1.0.0) (2023-12-01)


### Bug Fixes

* only run 1 instance ([03c97be](https://gitlab.com/rmnavz/lidarrspotdl/commit/03c97be9a6fbb5f64586b9111f2b4e6c87ef3842))
* pipeline ([1dc80e7](https://gitlab.com/rmnavz/lidarrspotdl/commit/1dc80e72740640d11f5313f82fbbc26f57d5caac))
