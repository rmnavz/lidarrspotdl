import os
import requests
import sys

def import_downloaded_tracks(downloaded_directories):
    lidarr_url = os.getenv("LIDARR_URL", "http://your-lidarr-ip:port")
    api_key = os.getenv("API_KEY", "your-lidarr-api-key")

    for download_dir in downloaded_directories:
        # GET request to retrieve data
        get_url = f"{lidarr_url}/api/v1/manualimport?apiKey={api_key}&folder={download_dir}&filterExistingFiles=true&replaceExistingFiles=false"
        response_get = requests.get(get_url)

        # Check if the GET request was successful (status code 200)
        if response_get.status_code == 200:
            data_to_post = response_get.json()  # Assuming the response is in JSON format
        else:
            print(f"Failed to retrieve data for {download_dir}. Status code: {response_get.status_code}")
            continue

        # Loop through the data and send a POST request for each entry
        for entry in data_to_post:
            post_url = f"{lidarr_url}/api/v1/manualimport?apiKey={api_key}"
            
            # Extract relevant data for the POST request
            post_data = {
                "id": entry["id"],
                "path": entry["path"],
                "artistId": entry["artist"]["id"],
                "albumId": entry["album"]["id"],
                "albumReleaseId": entry["albumReleaseId"],
                "trackIds": [track["id"] for track in entry["tracks"]],
                "quality": entry["quality"],
                "releaseGroup": "SpotDL",  # Set releaseGroup to "SpotDL"
                "additionalFile": entry["additionalFile"],
                "replaceExistingFiles": entry["replaceExistingFiles"],
                "disableReleaseSwitching": entry["disableReleaseSwitching"]
            }

            response_post = requests.post(post_url, json=post_data)

            # Check the response status
            if response_post.status_code == 200:
                print(f"Successfully imported: {entry['path']}")
            else:
                print(f"Failed to import: {entry['path']}. Status code: {response_post.status_code}")

# Retrieve the list of downloaded directories from command-line arguments
downloaded_directories = sys.argv[1:]

# Run the import function
import_downloaded_tracks(downloaded_directories)
