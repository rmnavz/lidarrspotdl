# Use an official Python runtime as a parent image
FROM python:3.8-slim

# Set the working directory in the container
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN pip install spotdl requests

# Copy the current directory contents into the container at /app
COPY . /app

# Make port 80 available to the world outside this container
EXPOSE 80

# Run run_spotdl.sh when the container launches
CMD ["sh", "-c", "/app/run_spotdl.sh"]


# Use an official Python runtime as a parent image
FROM python:3.8-slim

# Set the working directory in the container
WORKDIR /app

# Install any needed packages specified in requirements.txt
RUN apt-get update && apt-get install -y cron && rm -rf /var/lib/apt/lists/*
RUN pip install spotdl requests
RUN spotdl --download-ffmpeg

# Copy the current directory contents into the container at /app
COPY . /app

# Copy run_spotdl.sh script
COPY run_spotdl.sh /app/run_spotdl.sh
RUN chmod +x /app/run_spotdl.sh

# Copy setup_cron.sh script
COPY setup_cron.sh /app/setup_cron.sh
RUN chmod +x /app/setup_cron.sh

# Run setup_cron.sh when the container launches
CMD ["/app/setup_cron.sh"]
